# Ansible Notes

## Ansible Inventory
There is a requirement to always specify the hosts against which targets Ansible Playbooks (plays) are executed, both **_implicitly_** and **_explicitly_**.<br>

**You can:**<br>
1. **List** every host identified in "_The Inventory File_":<br>
   **ansible-playbook --list-hosts all** -or-<br>
   **ansible-playbook --list-hosts \***
2. **List** only those hosts identified by a "_group name_":<br>
   **ansible-playbook --list-hosts group_name**<br>
3. Run a specific **_Simple_ Ansible Module** against all hosts, such as **ping**:<br>
   **ansible-playbook all -m ping**
4. Run a specific **_Simple_ Ansible Module** against a "_group name_", such as **ping**:<br>
   **ansible-playbook webservers -m ping**
5. Run a specific **_Simple_ Ansible Module** against a host by name like **_webserver1_**, such as **ping**:<br>
   **ansible-playbook _webserver1_ -m ping**


## Ansible Config
Ansible needs configurations like any other application.  **Ansible Configurations** can be implemented in several ways and in a specific orderas listed below:<br>
1. **ANSIBLE_CONFIG**
2. Present working directory (where the **_ansible-playbook_** command is executed)
3. Homedir, of the user executing the **_ansible-playbook_** command
4. **/etc/ansible/ansible.cfg**

## Writing Ansible Playbooks
Ansible playbooks are broken down predominantly into modules to deliver changes.  Here are a few examples:
1. **file** (for delivering files, or file content such as in templates)
2. **ping** (ping so that you can validate a host is up or not)
3. **apt**  (a package manager for Debian variants)
4. **service** (to control daemons/services)

For a thorough list of [Ansible 2.9 Modules](https://docs.ansible.com/ansible/2.9/modules/modules_by_category.html) refer to the link.<br>
To prevent Ansible from loading specific modules refer to this link: [Rejecting Ansible Modules](https://docs.ansible.com/ansible/latest/user_guide/plugin_filtering_config.html)<br>

Ansible playbooks are written in YAML (Yet Another Markup Language), but consists of tasks that are broken down into names (titles) and modules (functions that enable changes).  However, at the top of all YAML scripts is the starting syntax of **---** (hyphens) and an ending, _but not required_, that looks like **```** (backticks).

**NOTE1:** You can verify your YAML syntax at this site [The YAML Validator](http://www.yamllint.com).<br>
**NOTE2:** You can hash out lines to comment them, and in YAML there is no such thing as multi-line commenting like there is in C/C++.


## Running Ansible Playbooks
When executing|running ansible playbooks there are four things to consider for appropriate labeling and addressing:

1. Name (banner)
2. hosts (targets of the tasks)
3. become (whether to escalate privilege via sudo or not)
4. tasks (what actions need to occur to make effective changes)

### Some notes:
1. In general playbooks are executed by typing the **ansible-playbook** command.
   a. As such you have to tell the command what hosts are the target of your changes either with the **-i** switch or specifying the "**hosts:**" parameter within the playbook.
   b. Also when you specify the "-name" parameter at the very top level of tasks, be prudent in what you "title" the task(s) so that you know what action is occuring when it is being executed.
2. If you want verbosity during the execution of a playbook you can also provide the **ansible-playbook** command the switches of **-v** or **-vvvv**.
3. You can run Ansible Playbooks for the explicit purpose of knowing what list of hosts are associated with a "_host group_" such as **_webservers_**, _not actually executing the tasks within it_.<br>
   a. Execute the following to accomplish this:<br>
      **ansible-playbook --list-hosts webservers**
4. You can also run Ansible Playbooks for the explicit purpose of know which hosts is "element 0" that is associated with a given "_host group_" such as **_webservers[0]_**, _not actually executing the tasks within it_.<br>
   a. Execute the following to accomplish this:<br>
      **ansible-playbook  --list-hosts webservers[0]**
5. Finally, you can run Ansible Playbooks for the explicit purpose of knowing which host **ARE NOT associated** with a given "_host group_" such as all but **_dbservers_**, _not actually executing the tasks within it_.<br>
   a. Execute the following to accomplish this:<br>
      **ansible-playbook  --list-hosts \\!dbservers**
6. Alternatively, you can run ad-hoc execution of commands on a selection of hosts (ex. webservers) by specifying the **-a** switch with the "_local_" command following.<br>
   a. Execute the following to accomplish this:<br>
      **ansible-playbook -a "uname -a" webservers**


## Ansible Directory Structure
The following is a **typical**, _not required_, **directory structure** for an ansible environment:

**/playbooks**
  - ansible.cfg
  - hosts
  - web-practice.yml
  - **/files**
    - nginx.conf
  - **/templates**
    - index.html.j2

> **NOTE:** _Playbooks_ can have:
>
>          1. vars_files:, and
>          2. pre_tasks:, and
>          3. handlers:

## Ansible Roles
#### Search for Ansible Roles by role_name
**ansible-galaxy search role_name --author author_name;**
> ansible-galaxy search ntp --author bennojoy
 - Gets/Downloads a specific Ansible Role by **role_name** by a **specific author**.

#### Display information about a specifically targeted Ansible Role
**ansible-galaxy info author_name.role_name**
> ansible-galaxy info bennojoy.ntp
 - Displays the specific details about (bennojoy.ntp).


#### Install a specifically targeted Ansible Role
**ansible-galaxy install author_name.role_name** like
> ansible-galaxy install bennojoy.ntp
 - Installs the specifically targeted Ansible Role (bennojoy.ntp).

#### List all installed Ansible Roles
**ansible-galaxy list** like
> ansible-galaxy list
 - List all **installed** Ansible Roles.

#### Remove, as opposed to install, a specifically targeted Ansible Role
**ansible-galaxy remove author_name.role_name** like
> ansible-galaxy remove bennojoy.ntp
 - Remove all content associated with the role (bennojoy.ntp).


## Ansible Variables
1. Can be inline in inventory,
2. Can be in ini Section [groupname:vars] for multiple hosts at a time,
3. Can be in vars.yml in a custom-defined directory.

```ansible-playbook  playbook.yml -e "somevar=somevalue"``` overwrites the value of _somevar_ from the **inventory**, **ini Section**, or **vars.yml** file. 


## Ansible Delegation
-Why?
-What?
-When?

## Ansible Tags
-Why?
-When?

## Ansible Lookups
-Why?
-When?
1. file
2. CSV file
3. ini file
4. pipe
5. env


## Ansible Prompting for values
#### With visible feedback at the prompt
```
- name:   username
  prompt: "Username? "
  default: "admin" <- To set a default answer for the prompt.
  ```

#### With no visible feedback at the prompt, for passwords as an example
```
- name: passwerd
  prompt: "Password? "
  private: yes  <- Makes User feedback invisible.
  encrypt: "sha512_crypt"  <- Possible values supported by Passlib, will be found https://docs.ansible.com/ansible/2.5/user_guide/playbooks_prompts.html
  confirm: yes  <- force prompt again to confirm matching password attempts.
  salt_size: _some_numerical_value_
```

**Note:** Execute ```pip install passlib``` to enable cryptography for Ansible.

## Ansible Vault
#### Ansible Vault Create
**ansible-vault create new_file.yml** <- your_name
 - uses EDITOR= value, defaults to **vi**.

#### Ansible Vault Encrypt 
**ansible-vault encrypt existing_file.yml** <- your_file
 - Encrypts existing_file.yml.

#### Ansible Vault View
**ansible-vault view existing_file.yml** <- your_file
 - Outputs file content of existing_file.yml.

#### Ansible Vault Edit
**ansible-vault edit existing_file.yml** <- your_name
 - Allows edits to existing_file.yml, then re-encrypts the file.

####  Ansible Vault Rekey
**ansible-vault rekey existing_file.yml** <- your_name
 - Allows you to change the password for access to existing_file.yml before allowing access to its content again!

#### Ansible Vault Decrypt
**ansible-vault decrypt existing_file.yml** <- your_name
 - Removes the password prompt and decrypts contents of the file.

##### Other Ansible Playbook Commands Related to Ansible-Vault
**ansible-playbook play_using_enc.yml --ask-vault-pass** <br>
**ansible-playbook play_using_enc.yml --vault-password-file  /path/to/saved_pass.txt**<br>
**Note:** *You create /path/to/saved_pass.txt*

## Ansible Start and Step ("Debugging")
**Purpose**: <br>
You can skip, on the cli, to a specific _named_ task within a containing playbook using the following command: <br>
**ansible-playbook play.yml --start-at-task="the_name"**

You can also step through playbooks to debug playbooks' tasks using the following command: <br>
**ansible-playbook play.yml --step** <br>
and react with a response of:
 1) decide not to after all (n)of
 2) specifically each one, individually (y)es
 3) or all the rest (c)ontinue


## Ansible Gather Facts  Optimization
**Purpose**: <br>
During the **Gathering of Facts Process**, there is a general slow down of overeall execution and as such these impedements can be **selectively disabled** at each targeted **Ansible Task** within a playbook by adding the following syntax below "**become:** " and "**hosts:** ": <br>

> gather_facts: false


## Ansible Check Mode ("Dry Run")
#### Ansible Syntax Check
**ansible-playbook play.yml  --syntax-check**

#### Ansible List Hosts
**ansible-playbook play.yml  --list-hosts**

#### Ansible List Tasks
**ansible-playbook play.yml  --list-tasks**

#### Ansible Simply Check
**ansible-playbook play.yml  --check**

#### Ansible Produce Differences of Changes
**ansible-playbook play.yml  --diff**
